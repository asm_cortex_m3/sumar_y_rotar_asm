/*
 * rotarbits.h
 *
 *  Created on: Sep 15, 2021
 *      Author: Usuario
 */

#ifndef INC_ROTARBITS_H_
#define INC_ROTARBITS_H_
#include <stdint.h>
uint32_t Sumar(int32_t x, int32_t y);
uint32_t RotarDerecha(uint32_t x, uint32_t b);
uint32_t RotarIzquierda(uint32_t, uint32_t b);

#endif /* INC_ROTARBITS_H_ */
